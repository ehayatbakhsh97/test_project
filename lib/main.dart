import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'Core/Config/routes.dart';

void main() {
  if (Platform.isAndroid || Platform.isIOS) {
    WidgetsFlutterBinding.ensureInitialized();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent, // transparent status bar
      statusBarBrightness: Platform.isIOS ? Brightness.dark : Brightness.light,
      statusBarIconBrightness:
          Platform.isIOS ? Brightness.dark : Brightness.light,
    ));
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
        .then((_) {
      runApp(const MyApp());
    });
  } else {
    runApp(const MyApp());
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Nojoum',
      theme: ThemeData(
        primaryColor: Colors.green,
        fontFamily: 'sora',
        colorScheme: const ColorScheme.light(primary: Colors.green),
        textTheme: const TextTheme(
          headline1: TextStyle(
            fontSize: 20,
            color: Colors.black,
          ),
          headline2: TextStyle(
            fontSize: 36,
            color: Colors.black,
          ),
          
        ),
      ),
      routes: Routes().appRoutes,
      initialRoute: '/help',
      // home: const HelpScreen(),
    );
  }
}
