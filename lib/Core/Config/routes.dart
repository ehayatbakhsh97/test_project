import 'package:flutter/material.dart';
import '../../Feature/Help/Screen/help.dart';

class Routes {
  static const help = '/help';

  final Map<String, Widget Function(BuildContext)> appRoutes = {
    Routes.help: (ctx) => const HelpScreen(),
  };
}
