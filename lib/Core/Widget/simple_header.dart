import 'package:flutter/material.dart';

class SimpleHeader extends StatelessWidget {
  final String mainHeader;
  const SimpleHeader({
    Key? key,
    required this.mainHeader,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final themeData = Theme.of(context);
    return LayoutBuilder(
      builder: (ctx, cons) => Padding(
        padding: const EdgeInsets.only(left: 20, right: 10),
        child: ConstrainedBox(
          constraints: BoxConstraints(maxWidth: cons.maxWidth),
          child: Row(
            children: [
              Text(
                mainHeader.toUpperCase(),
                style: themeData.textTheme.headline2,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
