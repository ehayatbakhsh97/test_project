import 'package:flutter/material.dart';

class SubmitButton extends StatelessWidget {
  final Function func;
  final Color color;
  final Color fontColor;

  final String title;
  final double fontSize;
  final double borderRadius;

  const SubmitButton({
    Key? key,
    required this.func,
    required this.title,
    required this.color,
    required this.fontColor,
    this.fontSize = 16,
    this.borderRadius = 10,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => func(),
      child: Container(
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(borderRadius),
          // border: Border.all(color: color),
        ),
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
        child: Center(
          child: Text(
            title,
            textAlign: TextAlign.end,
            style: TextStyle(color:fontColor ),
          ),
        ),
      ),
    );
  }
}
