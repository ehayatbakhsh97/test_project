import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../Model/faq.dart';

class FAQProvider extends ChangeNotifier with ReassembleHandler {
  bool _letExpandMore = false;
  get letExpandMore => _letExpandMore;

  bool _letShowAll = false;
  get letShowAll => _letShowAll;

  late int maxCollapseValue;
  TextEditingController textCtrl = TextEditingController();
  ScrollController scrollCtrl = ScrollController();
  final FAQGroupModel faqDatas;

  FAQProvider(this.faqDatas) {
    if (faqDatas.items.length > 3) {
      _letExpandMore = true;
      maxCollapseValue = 3;
    } else {
      _letExpandMore = false;
      maxCollapseValue = faqDatas.items.length;
    }
  }
  changeLetExpandMore() {
    _letExpandMore = false;
    _letShowAll = true;
    notifyListeners();
  }

  fetchDatas(BuildContext context) async {}

  @override
  void reassemble() {}
}
