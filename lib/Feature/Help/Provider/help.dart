import 'dart:convert';

// import 'package:either_dart/either.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:testproject/Feature/Help/Model/faq.dart';

// import '../../../Core/Model/error_result.dart';
// import '../../../Core/Model/server_request.dart';

class HelpProvider extends ChangeNotifier with ReassembleHandler {
  bool isLoading = true;
  bool isLoadingMore = false;

  TextEditingController textCtrl = TextEditingController();
  ScrollController scrollCtrl = ScrollController();
  int currentPage = 1;
  bool lockPage = false;
  List<FAQGroupModel> datas = [];

  fetchDatas(BuildContext context) async {
    const String apiResult = '''[
            {
                "_id": "617d33ab4ec44f591ce7470b",
                "name": "This is a anothersdw",
                "order": 0,
                "faqs": [
                    {
                        "_id": "61c75d6da2cf6dbb53e82a0e",
                        "title": "asd",
                        "description": "asdasd",
                        "order": 1
                    },
                    {
                        "_id": "61d5c5215c6e9c0a442bb224",
                        "title": "testtttt",
                        "description": "testdrtsikmc",
                        "order": 1
                    },
                    {
                        "_id": "61deca8016adb9b2b7253468",
                        "title": "sdw",
                        "description": "asdw",
                        "order": 2
                    },
                    {
                        "_id": "61deceaa16adb9b2b7253648",
                        "title": "sdw",
                        "description": "sdw",
                        "order": 3
                    }
                ],
                "faqsCount": 4
            },
            {
                "_id": "617d34168cd9a5088aeaff62",
                "name": "Hello worldsw",
                "order": 2,
                "faqs": [
                    {
                        "_id": "61c74defa2cf6dbb53e8286e",
                        "title": "testing",
                        "description": "asdasda",
                        "order": 1
                    },
                    {
                        "_id": "61c74df4a2cf6dbb53e82873",
                        "title": "asdwas",
                        "description": "asdwa",
                        "order": 1
                    }
                ],
                "faqsCount": 2
            }
        ]''';
    var values = json.decode(apiResult);
    values.forEach((element) {
      datas.add(FAQGroupModel.fromJson(element));
    });
    notifyListeners();
    // final Either<ErrorResult, dynamic> result = await ServerRequest().fetchData(
    //   'Urls.login',
    // );
    // result.fold(
    //   (error) async {
    //     await ErrorResult.showDlg(error.type!, context);
    //     isLoading = false;
    //     notifyListeners();
    //   },
    //   (result) {
    //     // result['data'].forEach((element) {
    //     //   blogs.add(BlogModel.fromJson(element));
    //     // });
    //     isLoading = false;
    //     notifyListeners();
    //   },
    // );
  }

  @override
  void reassemble() {}
}
