import '../../../Core/Model/global.dart';

class FAQGroupModel {
  final String id, name;
  final int order, count;
  final List<FAQItemModel> items;
  FAQGroupModel({
    required this.id,
    required this.name,
    required this.order,
    required this.count,
    required this.items,
  });

  factory FAQGroupModel.fromJson(Map datas) {
    List<FAQItemModel> faqs = [];
    datas['faqs'].forEach((element) {
      faqs.add(FAQItemModel.fromJson(element));
    });
    return FAQGroupModel(
      id: GlobalEntity.dataFilter(datas['_id'].toString()),
      name: GlobalEntity.dataFilter(datas['name'].toString()),
      order: datas['order'],
      count: datas['faqsCount'],
      items: faqs,
    );
  }
}

class FAQItemModel {
  final String id, title, description;
  final int order;

  FAQItemModel({
    required this.id,
    required this.title,
    required this.description,
    required this.order,
  });

  factory FAQItemModel.fromJson(Map datas) {
    return FAQItemModel(
      id: GlobalEntity.dataFilter(datas['_id'].toString()),
      title: GlobalEntity.dataFilter(datas['title'].toString()),
      description: GlobalEntity.dataFilter(datas['description'].toString()),
      order: datas['order'],

    );
  }
}
