import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../Provider/help.dart';
import '../Widget/contact_button.dart';
import '../Widget/faq.dart';
import '../Widget/tab_bar.dart';

class HelpScreen extends StatelessWidget {
  const HelpScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<HelpProvider>(
      create: (ctx) => HelpProvider(),
      child: const HelpTile(),
    );
  }
}

class HelpTile extends StatefulWidget {
  const HelpTile({
    Key? key,
  }) : super(key: key);

  @override
  _HelpTileState createState() => _HelpTileState();
}

class _HelpTileState extends State<HelpTile> {
  @override
  void initState() {
    Future.microtask(() =>
        Provider.of<HelpProvider>(context, listen: false).fetchDatas(context));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;
    return Consumer<HelpProvider>(
      builder: (ctx, provider, _) => SafeArea(
        child: DefaultTabController(
          length: 3,
          child: Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              backgroundColor: Colors.white,
              elevation: 1,
              centerTitle: true,
              title: Text(
                'Help Centre',
                style: textTheme.headline1,
              ),
              bottom: PreferredSize(
                preferredSize: const Size(double.infinity, 100),
                child: HelpTabBar(theme: theme),
              ),
            ),
            bottomSheet: SizedBox(
              height: 100,
              child: ContactButton(
                func: () {},
                title: 'Contact Us',
                color: Colors.black,
                fontColor: const Color(0XFFEED448),
              ),
            ),
            body: LayoutBuilder(
              builder: (ctx, cons) => TabBarView(
                children: [
                  FAQBox(
                    theme: theme,
                    provider: provider,
                  ),
                  const Center(child: Text('. . .')),
                  const Center(child: Text('. . .'))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
