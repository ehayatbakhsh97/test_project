import 'package:flutter/material.dart';
import '../../../Core/Widget/input_box.dart';

class HelpTabBar extends StatelessWidget {
  const HelpTabBar({
    Key? key,
    required this.theme,
  }) : super(key: key);

  final ThemeData theme;

  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: [
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          height: 40,
          child: InputBox(
            icon: Icons.search,
            label: 'Search',
            controller: TextEditingController(),
          ),
        ),
        const SizedBox(height: 10),
        TabBar(
          unselectedLabelColor: Colors.grey[500],
          indicatorColor: theme.primaryColor,
          labelColor: theme.primaryColor,
          tabs: const [
            Tab(
                icon: Text(
              'FAQ',
              style: TextStyle(fontSize: 12),
            )),
            Tab(
                icon: Text(
              'Terms & Conditions',
              style: TextStyle(fontSize: 11),
            )),
            Tab(
                icon: Text(
              'Privacy & Policy',
              style: TextStyle(fontSize: 12),
            )),
          ],
        ),
      ],
    );
  }
}
