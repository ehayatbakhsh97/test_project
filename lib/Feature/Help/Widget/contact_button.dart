import 'package:flutter/material.dart';

class ContactButton extends StatelessWidget {
  final Function func;
  final Color color;
  final Color fontColor;

  final String title;
  final double fontSize;
  final double borderRadius;

  const ContactButton({
    Key? key,
    required this.func,
    required this.title,
    required this.color,
    required this.fontColor,
    this.fontSize = 16,
    this.borderRadius = 15,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.all(20.0),
      child: InkWell(
        onTap: () => func(),
        child: Container(
          decoration: BoxDecoration(
            color: color,
            borderRadius: BorderRadius.circular(borderRadius),
            // border: Border.all(color: color),
          ),
          padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 5),
          child: Center(
            child: Text(
              title,
              textAlign: TextAlign.end,
              style: TextStyle(color: fontColor, fontSize: 22),
            ),
          ),
        ),
      ),
    );
  }
}
