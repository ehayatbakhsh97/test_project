import 'package:flutter/material.dart';

import '../../../Core/Widget/simple_header.dart';
import '../Provider/help.dart';
import 'faq_group.dart';

class FAQBox extends StatelessWidget {
  const FAQBox({
    Key? key,
    required this.theme,
    required this.provider,
  }) : super(key: key);

  final ThemeData theme;
  final HelpProvider provider;

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        const SizedBox(height: 20),
        const SimpleHeader(
          mainHeader: '''Getting
Started''',
        ),
        const SizedBox(height: 40),
        ListView.separated(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemBuilder: (ctx, ind) => FAQGroupBox(
            faqGroupdata: provider.datas[ind],
            themeData: theme,
          ),
          separatorBuilder: (ctx, ind) => const SizedBox(height: 40),
          itemCount: provider.datas.length,
        ),
        const SizedBox(height: 110),
      ],
    );
  }
}
