import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../Model/faq.dart';
import '../Provider/faq.dart';

class FAQGroupBox extends StatelessWidget {
  const FAQGroupBox({
    Key? key,
    required this.faqGroupdata,
    required this.themeData,
  }) : super(key: key);
  final FAQGroupModel faqGroupdata;
  final ThemeData themeData;

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<FAQProvider>(
      create: (ctx) => FAQProvider(faqGroupdata),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Consumer<FAQProvider>(
          builder: (ctx, provider, _) => Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                faqGroupdata.name,
                textAlign: TextAlign.left,
                style: TextStyle(
                  color: themeData.primaryColor,
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 20),
              ListView.separated(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (ctx, ind) => Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(color: const Color(0XFFE4E4E4)),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: ExpansionTile(
                      childrenPadding: const EdgeInsets.only(
                          left: 15, right: 15, bottom: 15),
                      // collapsedIconColor: secondFontColor,
                      // collapsedTextColor: secondFontColor,
                      backgroundColor: const Color(0XFFF8F8F8),
                      collapsedBackgroundColor: const Color(0XFFF8F8F8),
                      iconColor: Colors.black,
                      textColor: Colors.black,
                      title: Text(
                        '${faqGroupdata.items[ind].title} :',
                        textAlign: TextAlign.left,
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      children: [
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            faqGroupdata.items[ind].description,
                            textAlign: TextAlign.left,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                separatorBuilder: (ctx, ind) => const SizedBox(height: 20),
                itemCount: provider.letShowAll
                    ? faqGroupdata.items.length
                    : provider.maxCollapseValue,
              ),
              const SizedBox(height: 10),
              provider.letExpandMore
                  ? InkWell(
                      onTap: () {
                        provider.changeLetExpandMore();
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        child: Text(
                          'See all ${provider.faqDatas.items.length} articles',
                          textAlign: TextAlign.left,
                        ),
                      ),
                    )
                  : Container(),
            ],
          ),
        ),
      ),
    );
  }
}
